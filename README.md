art-slides
==========

This repository contains LaTeX/beamer slides related to the _art_
project.

Please keep to one LaTeX source file for each slide, or for a few
related slides.

The slideshow_*.tex files are full slideshows that group together
interesting collections of slides. These files should contain no new
slides themselves; they should only include other files, in an order
that makes sense. An exception is made for slides that are only
section dividers or generated tables-of-contents for sections.

Generating PDFs from the slides requires an up-to-date installation of
LaTeX and many LaTeX macro packages. It also requires add-on packages
available from https://github.com/marcpaterno/texmf.

